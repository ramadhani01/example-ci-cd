module.exports = {
  devServer: {
    proxy: {
      "/service-astra-na": {
        target: "http://192.168.20.223:8979/", //testing pakai PII-Humint
        // target: "http://192.168.210.32:8979/",
        secure: false,
        logLevel: "debug",
        pathRewrite: {
          "^/service-astra-na": "",
        },
        changeOrigin: true,
      },
    },
  },
  transpileDependencies: ["vuetify"],
};
