import { TokenService } from "@/services/modules/service.storage";

beforeEach(() => {
  sessionStorage.clear();
});

describe("TokenService", () => {
  it("getToken", () => {
    TokenService.getToken();
    expect(sessionStorage.getItem);
  });
  it("getSecret", () => {
    TokenService.getSecret();
    expect(sessionStorage.getItem);
  });
  it("getUid", () => {
    TokenService.getUID();
    expect(sessionStorage.getItem);
  });
  it("saveToken", () => {
    TokenService.saveToken("tkn1234");
    expect(sessionStorage.setItem);
  });
  it("saveSecret", () => {
    TokenService.saveSecret("usr001");
    expect(sessionStorage.setItem);
  });
  it("saveUID", () => {
    TokenService.saveUID("id001");
    expect(sessionStorage.setItem);
  });
  it("removeToken", () => {
    TokenService.saveToken("tkn1234");
    const KEY = "access_token";
    TokenService.removeToken(KEY);
    expect(sessionStorage.removeItem);
  });
  it("removeSecret", () => {
    TokenService.saveSecret("usr001");
    const KEY = "secret_key";
    TokenService.removeSecret(KEY);
    expect(sessionStorage.removeItem);
  });
  it("removeUid", () => {
    TokenService.saveUID("id001");
    const KEY = "uid";
    TokenService.removeUID(KEY);
    expect(sessionStorage.removeItem);
  });
  it("removeall", () => {
    TokenService.saveSecret("usr001");
    TokenService.saveToken("tkn1234");
    TokenService.saveSecret("id001");
    TokenService.removeAll();
    expect(sessionStorage.clear);
  });
});
