import { createLocalVue, shallowMount } from "@vue/test-utils";
import Vuex from "vuex";
import Dashboard from "@/components/Dashboard.vue";
import Transaksi from "@/views/Transaksi.vue";
import Login from "@/views/Login.vue";
import Vuetify from "vuetify";
import "../../setup";
import VueRouter from "vue-router";

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(VueRouter);

jest.useFakeTimers();

const routes = [
  {
    path: "/login",
    name: "Login",
    component: Login,
  },
  {
    path: "/add",
    name: "Transaksi",
    component: Transaksi,
  },
];

const router = new VueRouter({
  routes,
});

let response = {
  saldo: 150100,
  pemasukan: 150100,
  pengeluaran: 0,
  list_transaksi: [
    {
      id: "2f36e14163aed7cbbdd552b32d454c2d",
      name: "nemu uang di jalan",
      nominal: 100000,
      type: "pemasukan",
      timestamp: 1643735002781,
      id_user: "00dfc53ee86af02e742515cdcf075ed3",
    },
    {
      id: "0d590764266854f98f9102bc39283ce5",
      name: "nemu uang",
      nominal: 100,
      type: "pemasukan",
      timestamp: 1643733315940,
      id_user: "00dfc53ee86af02e742515cdcf075ed3",
    },
    {
      id: "7059cbb2f4a69bbad4e3375870592de7",
      name: "nabung",
      nominal: 50000,
      type: "pemasukan",
      timestamp: 1643374911439,
      id_user: "00dfc53ee86af02e742515cdcf075ed3",
    },
  ],
};

let response2 = [
  {
    name: "this is test",
    username: "budi",
    profession: "programer",
    email: "test@gmail.com",
    password: "rahasia2021",
    id: "00dfc53ee86af02e742515cdcf075ed3",
  },
];

describe("Transaksi.vue", () => {
  let vuetify;
  let store;
  let store2;
  let actions;
  let actions2;
  let state;
  let state2;
  beforeEach(() => {
    vuetify = new Vuetify();
    state = {
      loginStatus: 200,
      users: null,
    };
    state2 = {
      transaction: null,
      deleteStatus: null,
      createStatus: null,
      editStatus: null,
    };
    actions = {
      getUser: jest.fn().mockReturnValue(true),
    };
    actions2 = {
      getTransaction: jest.fn().mockReturnValue(true),
    };
    store = new Vuex.Store({
      modules: {
        user: {
          namespaced: true,
          state: state,
          actions: actions,
        },
      },
    });
    store2 = new Vuex.Store({
      modules: {
        transaction: {
          namespaced: true,
          state: state2,
          actions: actions2,
        },
      },
    });
  });

  it("getDatas action", async () => {
    const wrapper = shallowMount(Dashboard, {
      localVue,
      vuetify,
      store2,
      data: () => ({
        dataDashboard: [],
      }),
    });
    expect(wrapper.vm.getDatas());
    wrapper.setData({
      dataDashboard: response2[0],
    });
  });

  it("getDatasTf action", async () => {
    const wrapper = shallowMount(Dashboard, {
      localVue,
      vuetify,
      store2,
      data: () => ({
        datasetIncome: "",
        datasetOutcome: "",
        datasetSaldo: "",
        dataTransaction: [],
      }),
    });
    expect(wrapper.vm.getDatasTf());
    wrapper.setData({
      dataTransaction: response,
    });
    wrapper.setData({
      datasetSaldo: response.saldo,
    });
    wrapper.setData({
      datasetIncome: response.pemasukan,
    });
    wrapper.setData({
      datasetOutcome: response.pengeluaran,
    });
  });
  it("onClick action", async () => {
    const wrapper = shallowMount(Dashboard, {
      localVue,
      vuetify,
      data: () => ({
        dialog: false,
        dataDetail: null,
      }),
    });
    expect(
      wrapper.vm.onClick({
        id: "2f36e14163aed7cbbdd552b32d454c2d",
        name: "nemu uang di jalan",
        nominal: 100000,
        type: "pemasukan",
        timestamp: 1643735002781,
        id_user: "00dfc53ee86af02e742515cdcf075ed3",
      })
    );
  });
  it("logout action", async () => {
    const wrapper = shallowMount(Dashboard, {
      localVue,
      vuetify,
    });
    expect(wrapper.vm.logout());
    wrapper.setData({
      typeConfirm: "logout",
      statusConfirm: true,
    });
  });
  it("memanggil data method ConfirmAlert", () => {
    const mockRouter = {
      push: jest.fn(),
    };
    const wrapper = shallowMount(Dashboard, {
      data() {
        return {
          typeConfirm: "delete",
        };
      },
      localVue,
      store2,
      router,
      mocks: {
        $router: mockRouter,
      },
    });
    let val = "yes";
    wrapper.vm.ConfirmAlert(val);
    expect(mockRouter.push);
    let vale = "no";
    wrapper.vm.ConfirmAlert(vale);
  });
  it("memanggil data method toAdd", () => {
    const mockRouter = {
      push: jest.fn(),
    };
    const wrapper = shallowMount(Dashboard, {
      localVue,
      store2,
      router,
      mocks: {
        $router: mockRouter,
      },
    });
    let val = "yes";
    wrapper.vm.toAdd(val);
    expect(mockRouter.push);
  });
});
