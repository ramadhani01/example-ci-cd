import { shallowMount } from "@vue/test-utils";
import Component from "@/components/alert/ConfirmAlert";
import "../../../setup";
import Vuetify from "vuetify";

describe("ConfirmAlert.vue", () => {
  let vuetify;
  beforeEach(() => {
    vuetify = new Vuetify();
  });

  it("watch data type harus berubah", () => {
    const wrapper = shallowMount(Component, {
      vuetify,
    });
    wrapper.setData({ type: "failed" });
    expect(wrapper.vm.type).toEqual("failed");
  });

  it("method create harus dipanggil", () => {
    const wrapper = shallowMount(Component, {
      vuetify,
    });

    let dummy = { title: "CREATE", sub_title: "Yakin mau ditambah?" };

    wrapper.setData({ title: "CREATE" });
    wrapper.setData({ sub_title: "Yakin mau ditambah?" });
    wrapper.vm.create();
    expect(wrapper.vm.title).toEqual(dummy.title);
    expect(wrapper.vm.sub_title).toEqual(dummy.sub_title);
  });

  it("method edit harus dipanggil", () => {
    const wrapper = shallowMount(Component, {
      vuetify,
    });

    let dummy = { title: "UPDATE", sub_title: "Yakin mau diperbarui?" };

    wrapper.setData({ title: "UPDATE" });
    wrapper.setData({ sub_title: "Yakin mau diperbarui?" });
    wrapper.vm.edit();
    expect(wrapper.vm.title).toEqual(dummy.title);
    expect(wrapper.vm.sub_title).toEqual(dummy.sub_title);
  });

  it("method pass harus dipanggil", () => {
    const wrapper = shallowMount(Component, {
      vuetify,
    });

    let dummy = {
      title: "CHANGE PASSWORD",
      sub_title: "Yakin mau diperbarui?",
    };

    wrapper.setData({ title: "CHANGE PASSWORD" });
    wrapper.setData({ sub_title: "Yakin mau diperbarui?" });
    wrapper.vm.pass();
    expect(wrapper.vm.title).toEqual(dummy.title);
    expect(wrapper.vm.sub_title).toEqual(dummy.sub_title);
  });

  it("method delete harus dipanggil", () => {
    const wrapper = shallowMount(Component, {
      vuetify,
    });

    let dummy = { title: "DELETE", sub_title: "Yakin mau dihapus?" };

    wrapper.setData({ title: "DELETE" });
    wrapper.setData({ sub_title: "Yakin mau dihapus?" });
    wrapper.vm.delete();
    expect(wrapper.vm.title).toEqual(dummy.title);
    expect(wrapper.vm.sub_title).toEqual(dummy.sub_title);
  });

  it("method logout harus dipanggil", () => {
    const wrapper = shallowMount(Component, {
      vuetify,
    });

    let dummy = {
      title: "LOG OUT",
      sub_title: "Yakin mau keluar?",
    };

    wrapper.setData({ title: "LOG OUT" });
    wrapper.setData({ sub_title: "Yakin mau keluar?" });
    wrapper.vm.logout();
    expect(wrapper.vm.title).toEqual(dummy.title);
    expect(wrapper.vm.sub_title).toEqual(dummy.sub_title);
  });

  it("method closeDialog harus dipanggil", () => {
    const wrapper = shallowMount(Component, {
      vuetify,
    });

    let dummy = true;

    wrapper.vm.closeDialog(dummy);
  });

  it("method checkDialogType harus dipanggil", () => {
    const wrapper = shallowMount(Component, {
      vuetify,
    });

    wrapper.setData({ type: "create" });
    wrapper.vm.checkDialogType();
    expect(wrapper.vm.type).toEqual("create");

    wrapper.setData({ type: "edit" });
    wrapper.vm.checkDialogType();
    expect(wrapper.vm.type).toEqual("edit");

    wrapper.setData({ type: "password" });
    wrapper.vm.checkDialogType();
    expect(wrapper.vm.type).toEqual("password");

    wrapper.setData({ type: "delete" });
    wrapper.vm.checkDialogType();
    expect(wrapper.vm.type).toEqual("delete");

    wrapper.setData({ type: "logout" });
    wrapper.vm.checkDialogType();
    expect(wrapper.vm.type).toEqual("logout");
  });
});
