import { shallowMount } from "@vue/test-utils";
import Component from "@/components/alert/FlashAlert";
import "../../../setup";
import Vuetify from "vuetify";

describe("ConfirmAlert.vue", () => {
  let vuetify;
  beforeEach(() => {
    vuetify = new Vuetify();
  });

  it("watch data type harus berubah", () => {
    const wrapper = shallowMount(Component, {
      vuetify,
    });
    wrapper.setData({ type: "failed" });
    expect(wrapper.vm.type).toEqual("failed");
  });

  it("method create harus dipanggil", () => {
    const wrapper = shallowMount(Component, {
      vuetify,
    });

    wrapper.setData({
      icon_source: "success.svg",
      color: "#5CAA01",
      title: "SUCCESS!",
      sub_title: "Everything looks perfect!",
      action_text: "Continue",
    });

    wrapper.vm.success();

    expect(wrapper.vm.icon_source).toEqual("success.svg");
    expect(wrapper.vm.color).toEqual("#5CAA01");
    expect(wrapper.vm.title).toEqual("SUCCESS!");
    expect(wrapper.vm.sub_title).toEqual("Everything looks perfect!");
    expect(wrapper.vm.action_text).toEqual("Continue");
  });

  it("method warning harus dipanggil", () => {
    const wrapper = shallowMount(Component, {
      vuetify,
    });

    wrapper.setData({
      icon_source: "warning.svg",
      color: "#FFC26A",
      title: "WARNING!",
      sub_title: "Please don't do that!",
      action_text: "Understand",
    });

    wrapper.vm.warning();

    expect(wrapper.vm.icon_source).toEqual("warning.svg");
    expect(wrapper.vm.color).toEqual("#FFC26A");
    expect(wrapper.vm.title).toEqual("WARNING!");
    expect(wrapper.vm.sub_title).toEqual("Please don't do that!");
    expect(wrapper.vm.action_text).toEqual("Understand");
  });

  it("method danger harus dipanggil", () => {
    const wrapper = shallowMount(Component, {
      vuetify,
    });

    wrapper.setData({
      icon_source: "danger.svg",
      color: "#DE4444",
      title: "OOPS!",
      sub_title: "Something Went Wrong!",
      action_text: "Try Again",
    });

    wrapper.vm.danger();

    expect(wrapper.vm.icon_source).toEqual("danger.svg");
    expect(wrapper.vm.color).toEqual("#DE4444");
    expect(wrapper.vm.title).toEqual("OOPS!");
    expect(wrapper.vm.sub_title).toEqual("Something Went Wrong!");
    expect(wrapper.vm.action_text).toEqual("Try Again");
  });

  it("method info harus dipanggil", () => {
    const wrapper = shallowMount(Component, {
      vuetify,
    });

    wrapper.setData({
      icon_source: "info.svg",
      color: "#7BA0D5",
      title: "INFO",
      sub_title: "For Your Information",
      action_text: "Ok",
    });

    wrapper.vm.info();

    expect(wrapper.vm.icon_source).toEqual("info.svg");
    expect(wrapper.vm.color).toEqual("#7BA0D5");
    expect(wrapper.vm.title).toEqual("INFO");
    expect(wrapper.vm.sub_title).toEqual("For Your Information");
    expect(wrapper.vm.action_text).toEqual("Ok");
  });

  it("method errorLogin harus dipanggil", () => {
    const wrapper = shallowMount(Component, {
      vuetify,
    });

    wrapper.setData({
      icon_source: "danger.svg",
      color: "#DE4444",
      title: "OOPS!",
      sub_title: "The username or password is incorrect",
      action_text: "Try Again",
    });

    wrapper.vm.errorLogin();

    expect(wrapper.vm.icon_source).toEqual("danger.svg");
    expect(wrapper.vm.color).toEqual("#DE4444");
    expect(wrapper.vm.title).toEqual("OOPS!");
    expect(wrapper.vm.sub_title).toEqual(
      "The username or password is incorrect"
    );
    expect(wrapper.vm.action_text).toEqual("Try Again");
  });

  it("method checkDialogType harus dipanggil", () => {
    const wrapper = shallowMount(Component, {
      vuetify,
    });

    wrapper.setData({ type: "success" });
    wrapper.vm.checkDialogType();
    expect(wrapper.vm.type).toEqual("success");

    wrapper.setData({ type: "warning" });
    wrapper.vm.checkDialogType();
    expect(wrapper.vm.type).toEqual("warning");

    wrapper.setData({ type: "danger" });
    wrapper.vm.checkDialogType();
    expect(wrapper.vm.type).toEqual("danger");

    wrapper.setData({ type: "info" });
    wrapper.vm.checkDialogType();
    expect(wrapper.vm.type).toEqual("info");

    wrapper.setData({ type: "error_login" });
    wrapper.vm.checkDialogType();
    expect(wrapper.vm.type).toEqual("error_login");
  });

  it("method closeDialog", () => {
    const wrapper = shallowMount(Component, {
      vuetify,
    });

    wrapper.vm.closeDialog();
  });
});
