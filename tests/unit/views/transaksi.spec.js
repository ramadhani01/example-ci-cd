import { createLocalVue, shallowMount } from "@vue/test-utils";
import Vuex from "vuex";
import Transaksi from "@/views/Transaksi.vue";
import Vuetify from "vuetify";
import "../../setup";
import VueRouter from "vue-router";

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(VueRouter);

jest.useFakeTimers();

describe("Transaksi.vue", () => {
  let vuetify;
  beforeEach(() => {
    vuetify = new Vuetify();
  });

  it("menampilkan halaman", async () => {
    const wrapper = shallowMount(Transaksi, {
      localVue,
      vuetify,
    });
  });
});
