import { createLocalVue, shallowMount } from "@vue/test-utils";
import Vuex from "vuex";
import Login from "@/views/Login.vue";
import Register from "@/views/Register.vue";
import Dashboard from "@/views/Home.vue";
import Vuetify from "vuetify";
import "../../setup";
import VueRouter from "vue-router";

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(VueRouter);

jest.useFakeTimers();

const routes = [
  {
    path: "/register",
    name: "Register",
    component: Register,
  },
  {
    path: "/",
    name: "Home",
    component: Dashboard,
  },
];

const router = new VueRouter({
  routes,
});

describe("Login.vue", () => {
  let vuetify;
  let store1;
  let store2;
  let actions1;
  let actions2;
  let state;
  let state2;

  beforeEach(() => {
    vuetify = new Vuetify();
    state = {
      loginStatus: 200,
      users: null,
    };
    state2 = {
      loginStatus: 400,
      users: null,
    };
    actions1 = {
      loginAction: jest.fn().mockReturnValue(true),
    };
    actions2 = {
      loginAction: jest.fn().mockReturnValue(false),
    };
    store1 = new Vuex.Store({
      modules: {
        user: {
          namespaced: true,
          state: state,
          actions: actions1,
        },
      },
    });
    store2 = new Vuex.Store({
      modules: {
        user: {
          namespaced: true,
          state: state2,
          actions: actions2,
        },
      },
    });
  });

  it("menampilkan pesan login error", async () => {
    const store = store2;
    const wrapper = shallowMount(Login, {
      store,
      localVue,
      vuetify,
    });
    expect(wrapper.vm.showLoginMessage("failed"));
  });

  it("menampilkan pesan login success", async () => {
    const store = store1;
    const wrapper = shallowMount(Login, {
      store,
      localVue,
      vuetify,
    });
    wrapper.vm.showLoginMessage("success");
  });

  it("handleOrientationChange", async () => {
    const lock = jest.fn();
    window.screen.orientation = { lock };
    let orientation = window.screen.orientation.type;
    const wrapper = shallowMount(Login, {
      localVue,
      vuetify,
    });
    wrapper.setData({
      portrait: true,
      landscape: false,
    });
    window.screen.orientation.type = "portrait-primary";
    expect(wrapper.vm.handleOrientationChange());
    expect(wrapper.vm.$data.portrait).toBe(true);
    expect(wrapper.vm.$data.landscape).toBe(false);
    window.screen.orientation.type = "landscape-primary";
    expect(wrapper.vm.handleOrientationChange());
    expect(wrapper.vm.$data.portrait).toBe(false);
    expect(wrapper.vm.$data.landscape).toBe(true);
  });

  it("login true", () => {
    const store = store1;
    const wrapper = shallowMount(Login, {
      store,
      localVue,
      vuetify,
    });
    wrapper.setData({
      input: {
        username: "user1",
        password: "rahasia",
        router: "/",
      },
    });
    expect(wrapper.vm.login());
    wrapper.vm.showLoginMessage("success");
  });
  it("login false", () => {
    const store = store2;
    const wrapper = shallowMount(Login, {
      store,
      localVue,
      vuetify,
    });
    wrapper.setData({
      input: {
        username: "user1",
        password: "rahasia",
        router: "/",
      },
    });
    expect(wrapper.vm.login());
    wrapper.vm.showLoginMessage("failed");
  });
  it("closeAlert Action", () => {
    const store = store1;
    const mockRouter = {
      push: jest.fn(),
    };
    const wrapper = shallowMount(Login, {
      localVue,
      vuetify,
      store,
      router,
      mocks: {
        $router: mockRouter,
      },
    });
    wrapper.setData({
      dialog: true,
    });
    expect(wrapper.vm.closeAlert());
    expect(state.loginStatus).toBe(200);
    expect(mockRouter.push);
  });
  it("routeRegister Action", () => {
    const mockRouter = {
      push: jest.fn(),
    };
    const $route = {
      name: "Register",
    };
    const wrapper = shallowMount(Login, {
      localVue,
      vuetify,
      router,
      mocks: {
        $router: mockRouter,
      },
    });
    expect(wrapper.vm.routeRegister());
    expect(mockRouter.push);
    expect(wrapper.vm.$route.name).toBe($route.name);
  });
});
