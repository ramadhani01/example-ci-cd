import { createLocalVue, shallowMount } from "@vue/test-utils";
import Vuex from "vuex";
import Home from "@/views/Home.vue";
import Vuetify from "vuetify";
import "../../setup";
import VueRouter from "vue-router";

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(VueRouter);

jest.useFakeTimers();

describe("Home.vue", () => {
  let vuetify;
  beforeEach(() => {
    vuetify = new Vuetify();
  });

  it("menampilkan halaman", async () => {
    const wrapper = shallowMount(Home, {
      localVue,
      vuetify,
    });
  });
});
