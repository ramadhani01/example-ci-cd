import Vue from "vue";
import VueRouter from "vue-router";
import Login from "../views/Login.vue";
import Register from "../views/Register.vue";
import Home from "../views/Home.vue";
import Transaksi from "../views/Transaksi.vue";
import EditTransaksi from "../views/EditTransaksi.vue";
import { TokenService } from "@/services/modules/service.storage";

Vue.use(VueRouter);

const routes = [
  {
    path: "/login",
    name: "Login",
    component: Login,
    meta: {
      public: true,
      onlyWhenLoggedOut: true,
    },
    beforeEnter(to, from, next) {
      if (TokenService.getUID()) {
        next({ name: "About" });
      } else {
        next();
      }
    },
  },
  {
    path: "/register",
    name: "Register",
    component: Register,
    meta: {
      public: true,
      onlyWhenLoggedOut: true,
    },
    beforeEnter(to, from, next) {
      if (TokenService.getUID()) {
        next({ name: "About" });
      } else {
        next();
      }
    },
  },
  {
    path: "/",
    name: "Home",
    component: Home,
    meta: {
      requiresAuth: true,
    },
    beforeEnter(to, from, next) {
      if (!TokenService.getUID()) {
        next({ path: "/login" });
      } else {
        next();
      }
    },
  },
  {
    path: "/add",
    name: "Transaksi",
    component: Transaksi,
    meta: {
      requiresAuth: true,
    },
    beforeEnter(to, from, next) {
      if (!TokenService.getUID()) {
        next({ path: "/login" });
      } else {
        next();
      }
    },
  },
  {
    path: "/edit/:name&:type&:id&:nominal",
    name: "EditTransaksi",
    component: EditTransaksi,
    props: true,
    meta: {
      requiresAuth: true,
    },
    beforeEnter(to, from, next) {
      if (!TokenService.getUID()) {
        next({ path: "/login" });
      } else {
        next();
      }
    },
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
