import Vue from "vue";
import Vuex from "vuex";
import user from "./user-management";
import transaction from "./transactions";
import publicapi from "./publix";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    user,
    transaction,
    publicapi
  },
});
