import axios from "axios";

const state = {
  namespaced: true,
  resultdata: null,
  deleteStatus: null,
  createStatus: null,
  editStatus: null,
};
const mutations = {
  SET_DATA(state, data) {
    
    state.resultdata = data;
  },
  DELETE_DATA(state, data) {
    state.deleteStatus = data;
  },
  CREATE_DATA(state, data) {
    state.createStatus = data;
  },
  EDIT_DATA(state, data) {
    state.editStatus = data;
  },
};
const actions = {
  getdaTa({ commit }, data) {
    return new Promise((resolve, reject) => {
      axios
        .get(`https://api.restful-api.dev/objects`)
        .then((res) => {
          commit("SET_DATA", res?.data);
          resolve(res?.data);
        })
        .catch((err) => {
          resolve(err.response.status);
          reject(err.response.status);
        });
    });
  },
  deleteDATA({ commit }, data) {
    return new Promise((resolve, reject) => {
      axios
        .delete(`https://api.restful-api.dev/objects/${data}`)
        .then((res) => {
          commit("DELETE_DATA", res.status);
          resolve(res);
        })
        .catch((err) => {
          resolve(err.response.status);
          reject(err.response.status);
        });
    });
  },
  createDATA({ commit }, data) {
    return new Promise((resolve, reject) => {
      axios
        .post("https://api.restful-api.dev/objects", data)
        .then((res) => {
          commit("CREATE_DATA", res.status);
          resolve(res);
        })
        .catch((err) => {
          resolve(err.response.status);
          reject(err.response.status);
        });
    });
  },
  updateDATA({ commit }, data) {
    return new Promise((resolve, reject) => {
      axios
        .put("https://api.restful-api.dev/objects/", data)
        .then((res) => {
          commit("EDIT_DATA", res.status);
          resolve(res.status);
        })
        .catch((err) => {
          resolve(err.response.status);
          reject(err.response.status);
        });
    });
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
