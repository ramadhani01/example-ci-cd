import axios from "axios";

const state = {
  namespaced: true,
  transaction: null,
  deleteStatus: null,
  createStatus: null,
  editStatus: null,
};
const mutations = {
  SET_TRANSACTION(state, data) {
    state.transaction = data;
  },
  DELETE_TRANSACTION(state, data) {
    state.deleteStatus = data;
  },
  CREATE_TRANSACTION(state, data) {
    state.createStatus = data;
  },
  EDIT_TRANSACTION(state, data) {
    state.editStatus = data;
  },
};
const actions = {
  getTransaction({ commit }, data) {
    return new Promise((resolve, reject) => {
      axios
        .get(`/service-astra-na/simple-crud/get-transaksi?id=${data}`)
        .then((res) => {
          commit("SET_TRANSACTION", res.data.data);
          resolve(res);
        })
        .catch((err) => {
          resolve(err.response.status);
          reject(err.response.status);
        });
    });
  },
  deleteTransaction({ commit }, data) {
    return new Promise((resolve, reject) => {
      axios
        .delete(`/service-astra-na/simple-crud/delete-transaksi?id=${data}`)
        .then((res) => {
          commit("DELETE_TRANSACTION", res.status);
          resolve(res);
        })
        .catch((err) => {
          resolve(err.response.status);
          reject(err.response.status);
        });
    });
  },
  createTransaction({ commit }, data) {
    return new Promise((resolve, reject) => {
      axios
        .post("/service-astra-na/simple-crud/create-transaksi", data)
        .then((res) => {
          commit("CREATE_TRANSACTION", res.status);
          resolve(res);
        })
        .catch((err) => {
          resolve(err.response.status);
          reject(err.response.status);
        });
    });
  },
  updateTransaction({ commit }, data) {
    return new Promise((resolve, reject) => {
      axios
        .put("/service-astra-na/simple-crud/update-transaksi", data)
        .then((res) => {
          commit("EDIT_TRANSACTION", res.status);
          resolve(res.status);
        })
        .catch((err) => {
          resolve(err.response.status);
          reject(err.response.status);
        });
    });
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
