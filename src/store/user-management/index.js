import axios from "axios";

const state = {
  namespaced: true,
  loginStatus: null,
  registerStatus: null,
  users: null,
};
const mutations = {
  SET_USERS(state, data) {
    state.users = data;
  },
  LOGIN_STATUS(state, data) {
    state.loginStatus = data;
  },
  REGISTER_STATUS(state, data) {
    state.registerStatus = data;
  },
};
const actions = {
  loginAction({ commit }, data) {
    return new Promise((resolve, reject) => {
      axios
        .post("/service-astra-na/humint-management/login", data) 
        // .post("/service-astra-na/simple-crud/login", data)
        .then((res) => {
          commit("LOGIN_STATUS", res.status);
          commit("SET_USERS", res.data.data);
          resolve(res);
        })
        .catch((err) => {
          commit("LOGIN_STATUS", err.response.status);
          resolve(err.response.status);
          reject(err.response.status);
        });
    });
  },
  registerAction({ commit }, data) {
    return new Promise((resolve, reject) => {
      axios
        .post("/service-astra-na/simple-crud/create-user", data)
        .then((res) => {
          commit("REGISTER_STATUS", res.status);
          resolve(res);
        })
        .catch((err) => {
          resolve(err.response.status);
          reject(err.response.status);
        });
    });
  },
  getUser({ commit }, data) {
    return new Promise((resolve, reject) => {
      axios
        .get(`/service-astra-na/simple-crud/get-user?id=${data}`)
        .then((res) => {
          commit("SET_USERS", res.data.data);
          resolve(res);
        })
        .catch((err) => {
          resolve(err.response.status);
          reject(err.response.status);
        });
    });
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
