const TOKEN_KEY = "access_token";
const SECRET_KEY = "secret_key";
const UID = "uid";

const TokenService = {
  getToken() {
    return sessionStorage.getItem(TOKEN_KEY);
  },

  getSecret() {
    return sessionStorage.getItem(SECRET_KEY);
  },

  getUID() {
    return sessionStorage.getItem(UID);
  },

  saveToken(accessToken) {
    sessionStorage.setItem(TOKEN_KEY, accessToken);
  },

  saveSecret(secretKey) {
    sessionStorage.setItem(SECRET_KEY, secretKey);
  },

  saveUID(uid) {
    sessionStorage.setItem(UID, uid);
  },

  removeToken() {
    sessionStorage.removeItem(TOKEN_KEY);
  },

  removeSecret() {
    sessionStorage.removeItem(SECRET_KEY);
  },

  removeUID() {
    sessionStorage.removeItem(UID);
  },

  removeAll() {
    sessionStorage.clear();
  },
};

export { TokenService };
