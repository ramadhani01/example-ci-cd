[![pipeline status](https://git.blackeye.id/Tedy.Fazrin/training-devops/badges/master/pipeline.svg)](https://git.blackeye.id/Tedy.Fazrin/training-devops/-/commits/master)

[![coverage report](https://git.blackeye.id/Tedy.Fazrin/training-devops/badges/master/coverage.svg)](https://git.blackeye.id/Tedy.Fazrin/training-devops/-/commits/master)

# training-vue

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
